%define NEW_LINE 0xA
%define SPACE 0x20
%define TAB 0x9

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60                 ; 'exit' syscall number     
	syscall
    ret 


; Принимает указатель на нуль-терминированную строку, возвращает её длину 
string_length:
    xor rax, rax                ; to be sure that rax is zeroed or else it can be random
    .loop:                  
        cmp byte[rdi+rax], 0    ; check if the symbol - null-terminator 
        je .end                 ; if it is then go to the end
        inc rax                 ; if not then increase rax by 1 (go to the next symbol)
        jmp .loop               ; restart the loop
    .end:
        ret   


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                    ; push the string sddress
    call string_length           
    mov rdx, rax                ; move rax (string length) to rdx
    pop rsi                     ; get the string address off the stack to rsi
    mov rax, 1                  ; system call number should be stored in rax
    mov rdi, 1                  ; move descriptor for stdout to rdi 
    syscall 
    ret


; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax                ; make sure rax is zeroed
    push rdi                    ; push the symbol address
    mov rsi, rsp                ; set pointer to the stack
    pop rdi                     ; clear the stack
    mov rax, 1                  ; 'write' syscall identifier
    mov rdi, 1                  ; stdout file descriptor
    mov rdx, 1                  ; write 1 byte
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE       
    call print_char
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax                ; make sure rax is zeroed
    mov rax, rdi                ; move the number address to rax
    mov r8, rsp                 ; save stack pointer
    mov r9, 10                  ; using 10 for dividing later
    push 0                      ; push the end of the line to the stack
    .loop:
        xor rdx, rdx            ; rdx = 0 
        div r9                  ; divide 1 digit
        add rdx, '0'            ; convert the number to ASCII
        dec rsp                 ; decrease stack pointer by 1
        mov byte[rsp], dl       ; lower byte of rdx goes to the stack
        cmp rax, 0              ; check if rax = 0 
        jne .loop               ; if not zero go to another loop
        mov rdi, rsp            ; save stack pointer (the start of the line)
        push r8                 ; save initial stack pointer 
        call print_string       ; print our number
        pop r8              
        mov rsp, r8             ; get back to initial stack pointer
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; compare number to 0 
    jge print_uint              ; if above or equal go to print_uint
    push rdi                    ; save number for later
    mov rdi, '-'                ; print '-' before the number
    call print_char            
    pop rdi                     ; move number back
    neg rdi                     ; make number true complement
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:                  ; 1 - rdi, 2 - rsi
    xor r8, r8                 
    xor r9, r9
    xor rcx, rcx                
    .loop:          
        mov r8b, byte[rdi+rcx]  ; put a symbol from the 1st string to r8
        mov r9b, byte[rsi+rcx]  ; put a symbol from the 2nd string to r9
        cmp r8, r9              ; compare symbols
        jne .not_equal          ; if they're not equal return 0
        cmp r8, 0               ; else check if the symbol is null-terminator
        je .equal               ; if so return 1
        inc rcx                 ; move to the next symbol
        jmp .loop               ; restart loop
    .equal:                 
        mov rax, 1
        ret
    .not_equal:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax                ; make sure rax is zeroed
    mov rdx, 1                  ; the amount of bytes
    mov rdi, 0                  ; stdin file descriptor
    push rdi                    ; dec stack pointer and push 0 on the top
    mov rsi, rsp                ; put buffer address to rsi
    syscall                 
    pop rax                     ; pop the char
    ret 


; Принимает: адрес начала буфера, размер буфера - rdi, rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы: пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:         
    xor rcx, rcx                ; counter for loop
    mov r8, 0                   ; length of the word
    .loop:
        push rdi                ; buffer address
        push rsi                ; buffer size
        push rcx                ; make sure read_char function doesn't change rcx 
        call read_char          ; read char from the word
        pop rcx                 ; get back saved rcx
        pop rsi                 ; get back saved buffer size
        pop rdi                 ; get back saved buffer address
        cmp rax, SPACE          ; check if char is a space
        je .skip                ; read next char
        cmp rax, TAB            ; check if char is \t
        je .skip
        cmp rax, NEW_LINE       ; check if char is \n
        je .skip
        cmp rax, 0              ; check if it's a new line
        je .done
        mov [rdi+rcx], rax      ; move char to the buffer
        inc rcx                 ; increase counter by 1 
        inc r8                  ; increase word length by 1
        cmp rcx, rsi            ; check if buffer is full
        jge .overflow           ; stop and return 0
        jmp .loop               ; repeat 
    .skip:
        cmp rcx, 0              ; check if it is a begining of the string
        jne .done               ; if not then the word is over go to .done
        jmp .loop               ; else repeat 
    .overflow:
        xor rax, rax            
        ret 
    .done: 
        xor rax, rax
        mov [rdi+rcx], rax       ; save 0 to the end
        mov rax, rdi  
        mov rdx, r8 
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, в rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; number will be stored here
    xor rcx, rcx                ; save number length
    .loop:
        xor r8, r8 
        mov r8b, byte[rdi+rcx]  ; get symbol
        cmp r8b, '0'            ; check if it is below '0'
        jb .end                 
        cmp r8b, '9'            ; check if it is above '9'
        ja .end
        inc rcx                 ; increase length by 1
        sub r8b, '0'            ; remove zeroes
        imul rax, 10            ; multiply rax by 10
        add rax, r8             ; add to number new symbol
        jmp .loop
    .end:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'          ; check if number is negative 
    je .negative_number         ; if so go to .negative_number
    jmp parse_uint              ; else go to parse_uint
    .negative_number:
        inc rdi                 ; start parsing without '-'
        call parse_uint         ; parse the number
        inc rdx                 ; add extra 1 to length
        neg rax                 ; negative the result so the number will be two-complemented
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:                    ; 1 - rdi, 2 - rsi, 3 - rdx
    xor rax, rax                ; make sure rax is zeroed
    xor r9, r9                  ; for symbol of the string
    xor rcx, rcx                ; set counter for loop on 0
    .loop:
        mov r9b, byte[rdi+rcx]  ; save string symbol
        mov byte[rsi+rcx], r9b  ; save string symbol in buffer
        inc rcx                 ; increase counter by 1 
        cmp rcx, rdx
        jge .overflow
        cmp r9b, 0              ; check if it is the end of the string
        je .end                
        jmp .loop 
    .end: 
        mov rax, rcx             ; return string length
        ret
    .overflow:
        mov rax, 0
        ret 
